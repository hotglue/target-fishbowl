"""Fishbowl target sink class, which handles writing streams."""

import requests
from singer_sdk.sinks import BatchSink

import backoff

class FishbowlSink(BatchSink):
    """Fishbowl target sink class."""

    max_size = 1

    @property
    def url_base(self) -> str:
        instance = self.config.get("instance")
        url = f"http://{instance}/api"
        return url

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {"Content-Type": "text/plain"}
        return headers

    @property
    def auth(self) -> dict:
        """Return the Authorization header"""

        headers = {"Content-Type": "application/json"}
        body = {
            "appName": self.config.get("app_name"),
            "appId": self.config.get("app_id"),
            "username": self.config.get("username"),
            "password": self.config.get("password"),
        }

        url = f"{self.url_base}/login"
        response = self._request('POST',url=url, json=body, headers=headers)

        if response.status_code != 200:
            raise RuntimeError(
                f"Login error: {response.reason} \n Message: {response.text}"
            )
        else:
            token = response.json().get("token")
            return {"Authorization": f"Bearer {token}"}

    def logout(self, auth_header: dict) -> None:
        """Logs out from Fishbowl's server to avoid auth issues"""
        url = f"{self.url_base}/logout"
        requests.post(url=url, headers=auth_header)

    def process_line(self,record) -> str:
        row = []
        for key, value in record.items():
            if type(value) == type(0.0):
                row.append(str(value))
            else:
                row.append('"' + str(value) + '"')

        return ",".join(row)

    def process_record(self, record: dict, context: dict) -> None:

        if self.stream_name == "SalesOrder":
            so = eval(record.get("order_lines","[]"))
        else: so = []
        
        if so: 
            record.pop("order_lines")

        if not context.get("csv_headers"):
            headers = ['"' + key + '"' for key in record]
            headers = ",".join(headers)
            context["headers"] = headers
            if so: 
                headers = ['"' + key + '"' for key in so[0]]
                headers = ",".join(headers)
                context["headers"] += '\n'+headers


        if not context.get("records"):
            context["records"] = self.process_line(record).replace('\n',' ')
        else:
            context["records"] += "\n" + self.process_line(record).replace('\n',' ')
        
        for item in so: 
            context["records"] += "\n" + self.process_line(item).replace('\n',' ')

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""

        text_data = context["headers"] + "\n" + context["records"]

        stream_name = self.stream_name

        url = f"{self.url_base}/import/{stream_name}"

        headers = self.http_headers

        auth_headers = self.auth
        if auth_headers:
            headers.update(auth_headers)
        try: 
            self.logger.debug(f"PAYLOAD: {text_data.encode('utf-8')}")

            response = self._request('POST',url=url, data=text_data.encode('utf-8'), headers=headers)

            if response.status_code != 200:
                raise RuntimeError(
                    f"Error: {response.reason} \n Message: {response.text}"
                )
            self.logger.info(f"INFO : Records successfully imported to Fishbowl:{stream_name}")
        finally:        
            self.logout(auth_headers)


    @backoff.on_exception(backoff.expo, Exception, max_tries=5)
    def _request(self,method, **kwargs):
        response = requests.request(method, **kwargs)
        response.raise_for_status()
        return response