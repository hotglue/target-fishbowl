"""Fishbowl target class."""

from __future__ import annotations

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_fishbowl.sinks import FishbowlSink


class TargetFishbowl(Target):
    """Sample target for Fishbowl."""

    name = "target-fishbowl"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "instance",
            th.StringType,
            description="The URL instance for API's base_url",
            required=True,
        ),
        th.Property(
            "app_name",
            th.StringType,
            description="The App name registered and approved in the Fishbowl Client",
            required=True,
        ),
        th.Property("app_id", th.StringType, description="App id", required=True),
        th.Property(
            "username",
            th.StringType,
            description="Registered username on the Fishbowl's server",
            required=True,
        ),
        th.Property(
            "password", th.StringType, description="username's password", required=True
        ),
    ).to_dict()

    default_sink_class = FishbowlSink


if __name__ == "__main__":
    TargetFishbowl.cli()
